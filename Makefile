CHANGELOG	:= CHANGELOG.md
SOURCES		:= $(wildcard wai/*.py)
VERSION		:= $(shell git describe --tags | sed -e 's/^v//' -e 's/-/+/' -e 's/-/./g')
VERSION_FILE	:= wai/version.py
VERSION_IN_FILE := $(shell sed -n 's/^version = "\(.*\)"/\1/p' $(VERSION_FILE))
PACKAGES	:= dist/wai_client-$(VERSION)-py3-none-any.whl dist/wai-client-$(VERSION).tar.gz

all: $(PACKAGES)

.PHONY: $(VERSION_FILE)
$(VERSION_FILE):
	@echo Version: $(VERSION)
	@echo Version in file: $(VERSION_IN_FILE)
ifeq ($(VERSION),$(VERSION_IN_FILE))
	@echo No need to update $(VERSION_FILE)
else
	sed 's/^version = .*/version = "$(VERSION)"/' wai/version.py.tmpl > $(VERSION_FILE)
endif

testversion: $(VERSION_FILE)
	@echo Running version thing

$(PACKAGES): $(SOURCES) | $(VERSION_FILE)
	@echo Version: $(VERSION)
	python3 -m build

packages: $(PACKAGES)

# fails if version doesn't match release pattern, so as not to publish
# development version.  PyPi will reject local dev versions but better to
# catch it here.
.PHONY: checkversion
checkversion:
	@echo "Checking that $(VERSION) is a release version"
	@[[ $(VERSION) =~ ^([0-9]+\.)*[0-9]+$$ ]] || ( echo "It's not" ; false )
	@echo "Checking that $(VERSION) is in $(VERSION_FILE)"
	@grep -qw "$(VERSION)" "$(VERSION_FILE)"

publish-test: $(PACKAGES) checkversion
	python3 -m twine upload --repository wai-test $(PACKAGES)

publish: $(PACKAGES) checkversion
	python3 -m twine upload --repository wai $(PACKAGES)
